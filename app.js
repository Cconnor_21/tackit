const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const passport = require('passport');
const mysql = require('mysql');
const flash = require('connect-flash');
const session = require('express-session');
const path = require('path');

var connection = require('./db/conn');

const app = express();

//including static css and javascript files
app.use(express.static('public'));

//passport config
require('./config/passport')(passport);

//ejs
app.use(expressLayouts);
app.set('view engine', 'ejs');

//bodyparser
app.use(express.urlencoded({extended:false}));

//Express Session
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

//Connect flash
app.use(flash());

//global vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));


const PORT = 5000;

app.listen(PORT, console.log(`server running on port ${PORT}`));
