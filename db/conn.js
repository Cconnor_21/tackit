var mysql=require('mysql');

//connect to mysql
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'tackit',
  multipleStatements: true
});


connection.connect(function(err) {
  if(err){
    console.error('error connecting to mysql');
    return;
  }
  console.log('Connected to Mysql');
})

module.exports = connection;
