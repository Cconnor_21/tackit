DROP DATABASE tackit;

CREATE DATABASE tackit;

USE tackit;

CREATE TABLE users(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  name varchar(30) not null,
  email varchar(30) not null,
  password varchar(200) not null
);

CREATE TABLE boards(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  title varchar(30) not NULL,
  userId INT,
  FOREIGN KEY (userId) REFERENCES users(id)
);

CREATE TABLE notes(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  title varchar(30) not null,
  description varchar(300) not NULL,
  boardId INT NOT null,
  CONSTRAINT FK_NoteBoard FOREIGN KEY (boardId) REFERENCES boards(id)
);


SELECT * FROM users;
SELECT * FROM notes;
SELECT * FROM boards;


INSERT INTO boards(title, userId) VALUES("first board", 1);
INSERT INTO notes(title, description, boardId) VALUES("First note", "This is the description for the first note", 1);

SELECT * FROM notes WHERE boardId = 1;
