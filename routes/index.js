const express = require('express');
const router = express.Router();
const {ensureAuthenticated} = require('../config/auth');
const path = require('path');

//welcome page
router.get('/', (req, res) => res.render('login'));

const mysql =  require('mysql');

var connection  = require('../db/conn');

router.get('/modal', (req, res) => {
  console.log("test");
  var data = "data";
  res.render('dashboard', {data: 1});
});



router.post('/dashboard', ensureAuthenticated, (req, res) => {
  const {title, description} = req.body;
  //check required fields
  console.log(req.user.id);
  if(!title || !description)
  {
    res.redirect("/dashboard");
  }
  else{
    connection.query(`INSERT INTO notes(title, description, userId) VALUES('${title}', '${description}', ${req.user.id});`, (err,rows) => {
    if(err) throw err;
    //res.send(rows);
    res.redirect('/dashboard');
    });
  }
});



//Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => {
  var show = 'false';
  router.use(express.static('public'));
  var queries = [
    `SELECT * FROM boards WHERE userId = 1`, "SELECT * FROM notes WHERE boardId = 1"
  ];
  connection.query(queries.join(';'), function(err, results, fields){
  if(err) throw err;
  //res.send(rows);
  console.log(results[0]);
  res.render('dashboard', {
    boards:results[0],
    notes:results[1],
    user: req.user.id,
    name: req.user.name,
    show:show
  });
  });
});

//delete todo by id
router.get('/delete/:id', (req, res) => {
  connection.query(`DELETE FROM notes WHERE id = ${req.params.id}`, (err,rows) => {
  if(err) throw err;
  //res.send(rows);
  res.redirect('/dashboard');
  });
});


module.exports = router;
