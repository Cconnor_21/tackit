const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const mysql =  require('mysql');


//connect to mysql
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'tackit'
});

module.exports = function(passport) {
  passport.use(
    new LocalStrategy({usernameField: 'email'}, (email, password, done) => {
      //Match user

      connection.query(`SELECT id, name, email, password from Users where email = '${email}'`, function(error, results, fields){
        if(error) throw error;
        if(!results.length){
          return done(null, false, {message: 'That email is not registered'});
        }

        //Match password
        console.log(password);
        console.log(results[0].password);
        bcrypt.compare(password, results[0].password, (err, isMatch) => {
          if(err) throw err;
          console.log(isMatch);
          if(isMatch){
            return done(null, results);
          }else{
            return done(null, false, {message: 'Password incorrect'});
          }
        });

        passport.serializeUser((results, done) => {
          done(null, results[0].id);
        });

        passport.deserializeUser((id, done) => {
          connection.query(`SELECT * FROM users WHERE id = ${id};`, (err, rows) => {
            done(null, rows[0]);
          })
        });

      });


    })
  );

}
